import { useState } from "react";

export default function ChangeName(){
    const [name, setName] = useState('Jean');

    function toggle() {
        setName('Fido')
      }

      return(
        <>
        <button onClick={toggle}>click !</button>
        <p>coucou {name}</p>
        </>
      )

}