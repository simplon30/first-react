import { useState } from "react";

export default function Counter(){
    const [number, setNumber] = useState(0)

    function Moins() {
      if (number < 1) {
      } else {
        setNumber(number - 1)
      }
    }
    
    return(
        <p>
        <button onClick={Moins}>-</button>
        <span>{number}</span>
        <button onClick={() => setNumber(number + 1)}>+</button>
      </p>
    )

}