import { useState} from "react";

export default function Structure() {

    const [showPara, setShowPara] = useState(true)
    
    const [names, setNames] = useState(['Name 1', 'Name 3', 'Name 4']);

    function Switch(){
        setShowPara(!showPara);    
    }

    function addName() {
        setNames([...names, 'Name 5']);
    }

    // function loop() {
    //     let tab = [];
    //     for(const name of names) {
    //         tab.push(<p key={name}>{name}</p>);
    //     }
    //     return tab;
    // }

    return (
        <main>
            <h1>Je suis le Titre</h1>
            
            <button onClick={addName}>Add name</button>
            {names.map((item, index) =>
                <p key={index}>{item}</p>
            )}

            <button onClick={Switch}>Bot</button>
            {showPara && <p>blablabla</p> }
        </main>
    )

}